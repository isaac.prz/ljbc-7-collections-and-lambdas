package com.ljbc.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is a csv file reader, that accepts in {@link #readFile(String)} a csv file containing a header and (at least) 3 columns:
 * <ul>
 * <li> province name
 * <li> town name
 * <li> postal code (zip)
 * </ul>
 * @author Isaac Perez
 */
public class CsvReader {

	private static final Logger LOGGER = LogManager.getLogger(CsvReader.class);
	private List<ZipCode> zipCodes = new ArrayList<ZipCode>();

	public void readFile( final String fileName ) {
		// clean the list in case 
		zipCodes.clear();
		Scanner fileScanner = null;
		try {
			// creates an scanner
			fileScanner = new Scanner(CsvReader.class.getResourceAsStream(fileName));
			// sets the delimiter pattern, in this case we separate the files using 
			fileScanner.useDelimiter("\n");
			
			// skip the first line, because is the header 
			if(fileScanner.hasNext())
			{
				fileScanner.next();
			}
			// while is similar to for, but only needs a boolean condition to continue
			while (fileScanner.hasNext()) {
				// creates another scanner for the line
				Scanner lineScanner = new Scanner(fileScanner.next()).useDelimiter(",");
				String province = lineScanner.next();
				String town = lineScanner.next();
				// we could here use nextInt() but the file contains some lines without numbers in this position,
				// so the reading would fail. We can read the column as text instead and check 
				// with java patterns if it contains a valid integer
				String zip = lineScanner.next();
				lineScanner.close();
				// check if the zip string is only composed by digits, so we can convert it with Integer.valueOf
				if (zip.trim().matches("\\d+")) {
					zipCodes.add(new ZipCode(province, town, Integer.valueOf(zip)));
				}
			}
		} catch (Exception e) {
			LOGGER.error("unexpected error while opening or reading the file", e);
		} finally {
			// closes the scanner
			fileScanner.close(); 
		}
	}

	public List<ZipCode> getZipCodes() {
		return zipCodes;
	}
}
