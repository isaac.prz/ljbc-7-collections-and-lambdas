package com.ljbc.collections;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ZipCodeFromTown {
	private static final Logger LOGGER = LogManager.getLogger(ZipCodeFromTown.class);

	public static void main(String[] args) {
		// final, to mark that this variable is assigned once, and never changed again,
		// it's a good practice
		final CsvReader reader = new CsvReader();
		// we read the file from classpath
		reader.readFile("/zip-codes-spain.csv");

		// get the zip codes that has been already read 
		final List<ZipCode> zipCodes = reader.getZipCodes();

		// here we are trying to retrieve the first zip code for the town 'Moraleja del vino'

		// since java 8 we can use streams directly from a collection
		// you open it with stream() and manipulate it with a fluid interface
		zipCodes.stream()
		// you can use this method to filter out the items, the method accepts a Predicate function
		.filter(z -> z.getTown().equalsIgnoreCase("moraleja del vino"))
		// with this method you can transform the processed item into another one, you can also change the type. 
		.map(ZipCode::getZipCode)
		// this stops the stream as soon as it finds one element, and transform it into an Optional
		.findFirst()
		// if we found a match we will print it
		.ifPresent(LOGGER::debug);

		// the :: in LOGGER::debug means that we want to use the method debug from the object LOGGER. it would be the same as
		// .ifPresent( zip -> LOGGER.debug(zip));
	}
}
