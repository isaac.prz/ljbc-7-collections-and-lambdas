package com.ljbc.collections;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ZipCodesFromProvince {
	private static final Logger LOGGER = LogManager.getLogger(ZipCodesFromProvince.class);

	public static void main(String[] args) {
		// final, to mark that this variable is assigned once, and never changed again,
		// it's a good practice
		final CsvReader reader = new CsvReader();
		// we read the file from classpath
		reader.readFile("/zip-codes-spain.csv");

		// get the zip codes that has been already read 
		final List<ZipCode> zipCodes = reader.getZipCodes();

		// here we are trying to retrieve a set of postal codes from zamora province. The elements in a set are always unique, they cannot repeat
		final Set<Integer> zipCodesOfZamoraProvince = 
				// since java 8 we can use streams directly from a collection
				// you open it with stream() and manipulate it with a fluid interface
				zipCodes.stream()
				// you can use this method to filter out the items, the method accepts a Predicate function
				.filter(z -> z.getProvince().equalsIgnoreCase("zamora"))
				// with this method you can transform the processed item into another one, you can also change the type. 
				.map(ZipCode::getZipCode)
				// this collects the previous transformed elements into a set (because the list is unique). It finalizes the stream
				.collect(Collectors.toSet());

		// for each is another way to iterate a collection. It's similar to "for(zip : zipCodesOfZamoraProvince)LOGGER.debug(zip) "
		zipCodesOfZamoraProvince.forEach(LOGGER::debug);
		// the :: in LOGGER::debug means that we want to use the method debug from the object LOGGER. it would be the same as
		// zipCodesOfZamoraProvince.forEach( zip -> LOGGER.debug(zip));
	}
}
