package com.ljbc.collections;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CountZipCodesByProvince {
	private static final Logger LOGGER = LogManager.getLogger(CountZipCodesByProvince.class);

	public static void main(String[] args) {
		// final, to mark that this variable is assigned once, and never changed again,
		// it's a good practice
		final CsvReader reader = new CsvReader();
		// we read the file from classpath
		reader.readFile("/zip-codes-spain.csv");

		// get the zip codes that has been already read
		final List<ZipCode> zipCodes = reader.getZipCodes();

		// here we are trying to retrieve the name of all provinces and count how many postal codes they have

		// since java 8 we can use streams directly from a collection
		// you open it with stream() and manipulate it with a fluid interface
		final Map<String, Long> zipCountByProvince = zipCodes.stream()
				// the group the results by province name, collecting only the count into a map
				.collect(Collectors.groupingBy(ZipCode::getProvince, Collectors.counting()));

		// In this case as we are iterating a map 
		zipCountByProvince
			// to 
			.entrySet().stream()
			// you can sort the list by province name ascennding
			.sorted(Comparator.comparing(Map.Entry::getKey))
			//  print the entry key and value
			.forEach( entry -> LOGGER.debug("The province {} has {} postal codes", entry.getKey(), entry.getValue()));

	}
}
