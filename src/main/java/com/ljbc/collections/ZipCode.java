package com.ljbc.collections;

public class ZipCode {

	private final String province;
	private final String town;
	private final int zipCode;
	
	
	public ZipCode(String province, String town, int zipCode) {
		super();
		this.province = province;
		this.town = town;
		this.zipCode = zipCode;
	}
	public String getProvince() {
		return province;
	}
	public String getTown() {
		return town;
	}
	public int getZipCode() {
		return zipCode;
	}
	
	
	
}
