# Ljbc-7 collections and lambdas

Learn java by challenge lesson 7: You will learn in this lesson how to manipulate collections of data (List, Set, Map, ...) and how to power up their handling using  java lambdas   

## Theory

This is a lot of informacion you dont need to know everything at once, check it once you want to get deeper:
maybe you can start with this tutorials for beginners:
* [java-8-lambda-tutorial-beginning-with-lambda-und-streams at kevcodez](https://kevcodez.de/posts/2015-07-19-java-8-lambda-tutorial-beginning-with-lambda-und-streams/)
* [java-streams-for-beginners at medium](https://medium.com/javarevisited/java-streams-for-beginners-105ed3aa2f74)
* [java-lambda-beginners-guide at javabeginnerstutorial](https://javabeginnerstutorial.com/core-java-tutorial/java-lambda-beginners-guide/)
* [java8-stream-tutorial-examples at winterbe](https://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/)

### Collections

Learn about the most common types of collections:
 * [List](https://docs.oracle.com/javase/8/docs/api/java/util/List.html)
 * [Set](https://docs.oracle.com/javase/8/docs/api/java/util/Set.html)
 * [Map](https://docs.oracle.com/javase/8/docs/api/java/util/Map.html)
 * check [this course at baeldung](https://www.baeldung.com/java-collections) if you want to go depper into the collections


### Streams

Learn how you can operate with list efficiently after java-8 using streams
 * [Streams](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html)
 * [Streams small tutorial at baeldung](https://www.baeldung.com/java-8-streams)
 * [Streams complete tutorial at baeldung](https://www.baeldung.com/java-streams)

### Lambdas and functional programming in java
 
Additionally learn how to use the functional interfaces with the Stream and Optional 
 * [Functional programming in java](https://www.baeldung.com/java-functional-programming)
 * [Optional](https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html)
 * [Function](https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html)
 * [Predicate](https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html)
 * [Supplier](https://docs.oracle.com/javase/8/docs/api/java/util/function/Supplier.html)
 * [Consumer](https://docs.oracle.com/javase/8/docs/api/java/util/function/Consumer.html) 
 
## Examples

check the examples in this order:
 * ZipCodesFromProvince
 * ZipCodeFromTown
 * CountZipCodesByProvince

## Challenges

1. get the names of the towns, whose name contains an X or W  
1. get all the zip codes from the Murcia province, filtering out Cartagena (they dont like each other and dont want to belong to the province :D )
1. list all the towns from Zamora, they must be sorted, first in ascending order, then in descending order
1. get the names of the provinces, that contains a town contaning the name "moraleja" (upper or lower case). Print the name of the provinces and their "moraleja" towns
1. print the highest (max) postal code for every province
1. get the first and last town name sorted by name
1. get the first and last town name sorted by postal code
1. get the average number of towns per province

Remember there are several ways to do it. Research in internet first to get ideas. 

## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)

